package main

import (
	"code.google.com/p/go.net/html"
	"encoding/json"
	"errors"
	"os"
	"strings"
)

type Node html.Node

func (n Node) MarshalJSON() ([]byte, error) {
	v, err := n.MarshalJsonml()
	if err != nil {
		return nil, err
	}
	return json.Marshal(v)
}

func quote(s string) string {
	if strings.Contains(s, `"`) {
		return `'` + s + `'`
	}
	return `"` + s + `"`
}

func (n Node) MarshalJsonml() (interface{}, error) {
	switch n.Type {
	case html.ErrorNode:
		return nil, errors.New("jsonml: cannot render an ErrorNode node")

	case html.TextNode:
		return n.Data, nil

	case html.DocumentNode, html.ElementNode:
		o := make([]interface{}, 0, 2)
		if n.Type == html.DocumentNode {
			o = append(o, "")
		} else {
			o = append(o, strings.ToUpper(n.Data))
			if len(n.Attr) > 0 {
				r := make(map[string]string, 0)
				for _, v := range n.Attr {
					if v.Namespace != "" {
						r[v.Namespace+":"+v.Key] = v.Val
					} else {
						r[v.Key] = v.Val
					}
				}
				o = append(o, r)
			}
		}

		for c := n.FirstChild; c != nil; c = c.NextSibling {
			v, err := (*Node)(c).MarshalJsonml()
			if err != nil {
				return nil, err
			}
			o = append(o, v)
		}
		return o, nil

	case html.CommentNode:
		return []string{"!", n.Data}, nil

	case html.DoctypeNode:
		s := "DOCTYPE " + n.Data
		if n.Attr != nil {
			var pub, sys *string
			for _, a := range n.Attr {
				switch a.Key {
				case "public":
					pub = &a.Val
				case "system":
					sys = &a.Val
				}
			}
			if pub != nil {
				s += " PUBLIC " + quote(*pub)
				if sys != nil {
					s += " " + quote(*sys)
				}
			} else if sys != nil {
				s += " SYSTEM " + quote(*sys)
			}
		}
		return []string{"!", s}, nil

	default:
		return nil, errors.New("jsonml: unknown node type")
	}
}

func main() {
	doc, err := html.Parse(os.Stdin)
	if err != nil {
		panic(err)
	}

	var buf []byte
	buf, err = json.Marshal((*Node)(doc))
	if err != nil {
		panic(err)
	}

	_, err = os.Stdout.Write(buf)
	if err != nil {
		panic(err)
	}
}
