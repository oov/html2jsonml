html2jsonml
===========

HTML to JsonML converter.

Usage
-----

```
$ echo '<html lang="en"><body><ul id=test><li>hello<li>world</ul></body></html>' | ./html2jsonml-linux-amd64
["",["HTML",{"lang":"en"},["HEAD"],["BODY",["UL",{"id":"test"},["LI","hello"],["LI","world"]],"\n"]]]
```
